# hw_1_modelling

Решение задачи мультилейбл классификации на примере [определения влияния людей на тропические леса](https://www.kaggle.com/c/planet-understanding-the-amazon-from-space/overview).

## Подготовка пайплайна

1. Подготовка окружения

```bash
python3 -m venv /path/to/new/virtual/environment
source /path/to/new/virtual/environment/bin/activate
```

2. Установка зависимостей

Находясь в активированном окружении, выполните:

```bash
pip install -r requirements.txt
```

## Запуск инференса

ClearML dashboard доступен по [ссылке](https://app.community.clear.ml/projects/65dd3f325bb14be8b70c4cdbcc87ad77/experiments/7f67a3e32828408392861b797ff64166/output/log)

1. Загрузим веса сети

```bash
cd /директория/проекта
dvc pull
```

2. Запустим ```predict.py```

```bash
python3 predict.py --model_path traced_model.pth --image_path tests/test_data/train_222.jpg
```

## Запуск обучения

1. Загрузка датасета

[Датасет доступен по ссылке](https://www.kaggle.com/c/planet-understanding-the-amazon-from-space/data)

Необходимо загрузить датасет и файл ```train_v2.csv```

2. В ноутбуке ```train_test_split.ipynb``` разбиваем датасет на части


3. В ```config.py``` меняем следующие поля:
```python
images_dir='/путь/до/директории/с/картинками'
train_dataset_path='/путь/до/файла/train_df.csv'
valid_dataset_path='/путь/до/файла/valid_df.csv'
test_dataset_path='/путь/до/файла/test_df.csv'
```

Остальные поля меняем по усмотрению

4. Настройка ClearML

- в своем профиле ClearML нажимаем "Create new credentials"
- у себя на компе, находясь в директории проекта, пишем ```clearml-init``` и следуем инструкциям

5. Запуск обучения

```bash
CUDA_VISIBLE_DEVICES=0 python3 train.py
```


## Запуск тестов

```bash
cd /директория/проекта
pytest
```
