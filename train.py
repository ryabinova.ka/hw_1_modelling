import logging
import typing as tp
from collections import OrderedDict

import torch
from catalyst import dl
from catalyst.core.callback import Callback
from clearml.logger import Logger

from src.callbacks import ClearMLCallback
from src.config import Config, config
from src.const import IMAGES, LOGITS, LOSS, PREDICTS, SCORES, TARGETS, VALID
from src.dataset import get_class_names, get_dataloaders
from src.logger import get_logger
from src.models import get_model
from src.train_utils import set_global_seed


def get_base_callbacks(
    local_config: Config,
    class_names: tp.List[str],
    local_logger: Logger,
    infer: bool = False,
) -> tp.List[Callback]:
    """Колбеки для трейна и теста.

    Args:
        local_config (Config): конфиг в виде объекта
        class_names (tp.List[str]): список с именами классов
        local_logger (Logger): логгер для clearml
        infer (bool): флаг для train и infer mode

    Returns:
        список колбеков
    """
    return [
        dl.BatchTransformCallback(
            transform=torch.sigmoid,
            scope='on_batch_end',
            input_key=LOGITS,
            output_key=SCORES,
        ),
        dl.BatchTransformCallback(
            transform=lambda score: score > local_config.binary_thresh,
            scope='on_batch_end',
            input_key=SCORES,
            output_key=PREDICTS,
        ),
        dl.AUCCallback(input_key=SCORES, target_key=TARGETS),
        dl.MultilabelPrecisionRecallF1SupportCallback(
            input_key=PREDICTS,
            target_key=TARGETS,
            num_classes=len(class_names),
            log_on_batch=False,
        ),
        dl.TracingCallback(
            input_key=IMAGES,
            logdir=local_config.checkpoints_dir,
            filename=local_config.traced_model_name,
        ),
        ClearMLCallback(local_logger, local_config, class_names, infer),
    ]


def get_train_callbacks(
    local_config: Config,
    class_names: tp.List[str],
    local_logger: Logger,
):
    """Колебеки для трейна.

    Args:
        local_config (Config): конфиг в виде объекта
        class_names (tp.List[str]): список с именами классов
        local_logger (Logger): логгер для clearml

    Returns:
        список колбеков
    """
    callbacks = get_base_callbacks(
        local_config,
        class_names,
        local_logger,
        infer=False,
    )
    callbacks.extend([
        dl.CriterionCallback(
            input_key=LOGITS,
            target_key=TARGETS,
            metric_key=LOSS,
        ),
        dl.OptimizerCallback(metric_key=LOSS),
        dl.SchedulerCallback(
            loader_key=VALID,
            metric_key=local_config.valid_metric,
        ),
        dl.CheckpointCallback(
            logdir=local_config.checkpoints_dir,
            loader_key=VALID,
            metric_key=local_config.valid_metric,
            minimize=local_config.minimize_metric,
        ),
        dl.EarlyStoppingCallback(
            patience=local_config.early_stop_patience,
            loader_key=VALID,
            metric_key=local_config.valid_metric,
            minimize=local_config.minimize_metric,
        ),
    ])
    return callbacks


def train(local_config: Config, local_logger: Logger) -> None:  # noqa: WPS210
    """Функция, в которой происходит обучение.

    Args:
        local_config (Config): конфиг в виде объекта
        local_logger (Logger): логгер для clearml
    """
    dataloaders_for_train, test_dataloader = get_dataloaders(local_config)
    class_names = get_class_names(local_config)
    model = get_model(
        len(class_names),
        **local_config.model_kwargs,
    )

    optimizer = local_config.optimizer(
        params=model.parameters(),
        **local_config.optimizer_kwargs,
    )
    scheduler = local_config.scheduler(
        optimizer=optimizer,
        **local_config.scheduler_kwargs,
    )

    if local_config.continue_from:
        model.load_state_dict(
            torch.load(local_config.continue_from)['model_state_dict'],
        )

    runner = dl.SupervisedRunner(
        input_key=IMAGES,
        output_key=LOGITS,
        target_key=TARGETS,
    )

    runner.train(
        model=model,
        criterion=local_config.loss,
        optimizer=optimizer,
        scheduler=scheduler,
        loaders=OrderedDict(dataloaders_for_train),
        callbacks=get_train_callbacks(local_config, class_names, local_logger),
        num_epochs=local_config.n_epochs,
        valid_loader=VALID,
        valid_metric=local_config.valid_metric,
        minimize_valid_metric=local_config.minimize_metric,
        seed=local_config.seed,
        verbose=True,
        load_best_on_end=True,
        amp=True,
        timeit=True,
    )

    runner.evaluate_loader(
        loader=test_dataloader,
        callbacks=get_base_callbacks(
            local_config,
            class_names,
            local_logger,
            infer=True,
        ),
        model=model,
        seed=local_config.seed,
        verbose=True,
    )


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    set_global_seed(seed=config.seed)
    logger = get_logger(config)
    train(config, logger)
