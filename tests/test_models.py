import torch

from src.base_config import Config
from src.models import get_model


def test_mobilenet_inference(test_config: Config) -> None:
    """Creates mobilenet model and than test its inference."""  # noqa: DAR101
    model = get_model(
        11,  # noqa: WPS432
        'mobilenetv3_small_075',
        pretrained=False,
    )
    model.eval()

    img_size = test_config.img_size
    input_tensor = torch.zeros((1, 3, img_size, img_size))

    output_tensor = model(input_tensor)
    assert output_tensor.shape == (1, 11)


def test_resnet_inference(test_config: Config) -> None:
    """Creates mobilenet model and than test its inference."""  # noqa: DAR101
    model = get_model(11, 'resnet18', pretrained=False)  # noqa: WPS432
    model.eval()

    img_size = test_config.img_size
    input_tensor = torch.zeros((1, 3, img_size, img_size))

    output_tensor = model(input_tensor)
    assert output_tensor.shape == (1, 11)
