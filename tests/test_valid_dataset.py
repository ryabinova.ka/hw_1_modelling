import numpy as np

from src.const import IMAGES, TARGETS


def test_image_open(dataset) -> None:
    """Test that we can open an image and checkout its shape."""
    result_dict = dataset[0]
    assert result_dict[IMAGES].shape == (3, 256, 256)
    assert result_dict[TARGETS].shape == (17, )


def test_element_type(dataset) -> None:
    """Test outputs types."""
    result_dict = dataset[0]
    assert isinstance(result_dict[IMAGES], np.ndarray)
    assert isinstance(result_dict[TARGETS], np.ndarray)
