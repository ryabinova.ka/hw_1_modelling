"""Testing module."""
import pandas as pd
from torch.utils.data import DataLoader

from src.config import augmentations
from src.dataset import PlanetDataset
from src.train_utils import preprocess_imagenet, worker_init_fn

from copy import deepcopy

import albumentations as alb
import pytest

from src.base_config import Config
from src.config import config


@pytest.fixture(scope='session')
def test_config() -> Config:
    local_config = deepcopy(config)
    local_config.train_dataset_path = 'tests/test_data/test_df.csv'
    local_config.valid_dataset_path = 'tests/test_data/test_df.csv'
    local_config.test_dataset_path = 'tests/test_data/test_df.csv'
    local_config.images_dir = 'tests/test_data'
    local_config.checkpoints_dir = 'tests_checkpoints'
    local_config.augmentations = alb.RandomBrightnessContrast(p=1)
    local_config.batch_size = 2

    return local_config


@pytest.fixture(scope='session')
def dataloader(test_config) -> DataLoader:
    """Fixture for dataloader.

    Returns:
        dataloader
    """
    df = pd.read_csv(test_config.train_dataset_path)
    dataset = PlanetDataset(
        df,
        test_config,
        augmentation=augmentations,
        preprocessing=preprocess_imagenet,
    )
    dataloader = DataLoader(
        dataset,
        test_config.batch_size,
        shuffle=False,
        num_workers=test_config.num_workers,
        worker_init_fn=worker_init_fn,
    )
    return dataloader


@pytest.fixture(scope='session')
def dataset(test_config: Config) -> PlanetDataset:
    """Создаем тестовый датасет."""
    df = pd.read_csv(test_config.train_dataset_path)
    return PlanetDataset(
        df,
        test_config,
        augmentations,
        preprocess_imagenet,
    )


@pytest.fixture(scope='session')
def dataset(test_config: Config) -> PlanetDataset:
    """Создаем тестовый датасет."""
    df = pd.read_csv(test_config.train_dataset_path)
    return PlanetDataset(
        df,
        test_config,
        augmentation=None,
        preprocessing=preprocess_imagenet,
    )
