import numpy as np

from src.base_config import Config
from src.const import IMAGES, TARGETS
from src.dataset import PlanetDataset


def test_image_open(test_config: Config, dataset: PlanetDataset) -> None:
    """Test that we can open an image and checkout its shape."""
    result_dict = dataset[0]
    img_size = test_config.img_size

    assert result_dict[IMAGES].shape == (3, img_size, img_size)
    assert result_dict[TARGETS].shape == (17, )


def test_element_type(dataset) -> None:
    """Test outputs types."""
    result_dict = dataset[1]
    assert isinstance(result_dict[IMAGES], np.ndarray)
    assert isinstance(result_dict[TARGETS], np.ndarray)
