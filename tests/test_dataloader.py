import pandas as pd
import pytest
import torch
from torch.utils.data import DataLoader

from src.base_config import Config
from src.config import augmentations, config
from src.const import IMAGES, TARGETS
from src.dataset import PlanetDataset
from src.train_utils import preprocess_imagenet, worker_init_fn


def test_types(dataloader: DataLoader):  # noqa: WPS442
    """Test types of batched images and targets.

    Args:
        dataloader (DataLoader): dataloader fixture
    """
    batch = next(iter(dataloader))
    images, targets = batch[IMAGES], batch[TARGETS]
    assert isinstance(images, torch.Tensor)
    assert isinstance(targets, torch.Tensor)


def test_shapes(test_config: Config, dataloader: DataLoader):  # noqa: WPS442
    """Test shapes of batched images and targets.

    Args:
        dataloader (DataLoader): dataloader fixture
    """
    batch = next(iter(dataloader))
    images, targets = batch[IMAGES], batch[TARGETS]

    assert images.shape == (
        test_config.batch_size,
        3,
        test_config.img_size,
        test_config.img_size,
    )
    assert targets.shape == (test_config.batch_size, 17)
