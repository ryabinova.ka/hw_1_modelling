import argparse

import cv2
import numpy as np
import torch

from src.train_utils import preprocess_imagenet

classes = np.array(
    [  # noqa: WPS317
        'agriculture',
        'artisinal_mine',
        'bare_ground',
        'blooming',
        'blow_down',
        'clear',
        'cloudy',
        'conventional_mine',
        'cultivation',
        'habitation',
        'haze',
        'partly_cloudy',
        'primary',
        'road',
        'selective_logging',
        'slash_burn',
        'water',
    ],
)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='image predict')
    parser.add_argument(
        '--model_path',
        type=str,
        help='models path',
    )
    parser.add_argument(
        '--image_path',
        type=str,
        help='image_path',
    )
    args = parser.parse_args()

    image = cv2.imread(args.image_path)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = preprocess_imagenet(image, 256)  # noqa: WPS432

    model = torch.jit.load(args.model_path).cpu()
    logits = model(torch.Tensor(image)[None])
    probs = torch.sigmoid(logits)
    probs = probs.detach().squeeze().tolist()

    result_str = [f'{label}: {prob}' for prob, label in zip(probs, classes)]  # noqa: WPS221, E501

    print(result_str)  # noqa: WPS421
