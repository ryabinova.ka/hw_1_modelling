import timm
from torch import nn


def get_model(
    n_classes: int,
    name: str,
    pretrained: bool,
    **kwargs,
) -> nn.Module:
    """Function for model generation.

    Args:
        n_classes (int): number of classes
        name (str): model name from timm, i.g. mobilenetv3_small_075
        pretrained (bool): is needed to download weights
        **kwargs: additional dict  # noqa: RST210

    Returns:
        model object
    """
    return timm.create_model(
        name,
        pretrained,
        num_classes=n_classes,
        **kwargs,
    )
