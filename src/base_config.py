import json
import os
import typing as tp
from dataclasses import asdict, dataclass

import albumentations as albu
import torch
from torch.optim.optimizer import Optimizer


@dataclass
class Config:  # noqa: WPS306
    """Class for config."""
    num_workers: int
    seed: int
    loss: torch.nn.Module
    optimizer: type(Optimizer)
    optimizer_kwargs: tp.Mapping
    scheduler: tp.Any
    scheduler_kwargs: tp.Mapping
    preprocessing: tp.Callable
    img_size: int
    augmentations: albu.Compose
    batch_size: int
    n_epochs: int
    early_stop_patience: int
    experiment_name: str
    model_kwargs: tp.Mapping
    log_metrics: tp.List[str]
    binary_thresh: float
    valid_metric: str
    minimize_metric: bool
    images_dir: str
    train_dataset_path: str
    valid_dataset_path: str
    test_dataset_path: str
    valid_ratio: float
    project_name: str
    checkpoints_dir: str
    traced_model_name: str
    continue_from: tp.Optional[str] = None

    def to_dict(self) -> dict:
        """Converter to dict.

        Returns:
            dict
        """
        res = {}
        for key, local_value in asdict(self).items():
            try:
                if isinstance(local_value, torch.nn.Module):
                    res[key] = local_value.__class__.__name__
                elif isinstance(local_value, dict):  # noqa: WPS220
                    res[key] = json.dumps(local_value, indent=4)
                else:
                    res[key] = str(local_value)
            except Exception:
                res[key] = str(local_value)
        return res

    def __post_init__(self) -> None:
        """Checkpoint config."""
        self.checkpoints_dir = os.path.join(  # noqa: WPS601
            '../checkpoints',
            self.project_name,
            self.experiment_name,
        )
