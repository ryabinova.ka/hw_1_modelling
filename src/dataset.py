import os
import typing as tp

import albumentations as alb
import cv2
import numpy as np
import pandas as pd
from torch.utils.data import DataLoader, Dataset

from src.base_config import Config
from src.config import IMG_SIZE, augmentations
from src.const import IMAGES, TARGETS
from src.train_utils import preprocess_imagenet, worker_init_fn


class PlanetDataset(Dataset):
    """Dataset for retrieving images and labels."""
    def __init__(
        self,
        df: pd.DataFrame,
        config: Config,
        augmentation: tp.Optional[alb.Compose] = None,
        preprocessing: tp.Optional[tp.Callable] = None,
    ):
        """Инициализуем класс датасета.

        Args:
            df (pd.DataFrame): dataframe
            config (Config): config with consts
            augmentation (alb.Compose): augs
            preprocessing (tb.Callable): preprocessing function
        """
        self.df = df
        self.config = config
        self.augmentations = augmentation
        self.preprocessing = preprocessing

    def __len__(self) -> int:
        """Return length on a dataset.

        Returns:
            length of a dataframe which represents a dataset
        """
        return len(self.df)

    def __getitem__(self, idx: int) -> tp.Dict[str, np.ndarray]:
        """Получаем одну картинку и ее лейблы.

        Args:
            idx (int): number of the row in the dataframe

        Returns:
            dictionary with image and labels of an image
        """
        image_name = self.df.iloc[idx, 0]
        image_path = os.path.join(self.config.images_dir, f'{image_name}.jpg')

        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        if self.augmentations:
            image = self.augmentations(image=image)['image']
        if self.preprocessing:
            image = self.preprocessing(image, IMG_SIZE)

        targets = self.df.iloc[idx, 1:].astype(float).to_numpy()

        return {IMAGES: image, TARGETS: targets}


def get_class_names(config: Config) -> tp.List[str]:
    """Возвращает имена классов.

    Args:
        config (Config): configuration

    Returns:
        list of class names
    """
    df = pd.read_csv(config.train_dataset_path)
    labels = df.columns[1:]
    return labels.tolist()


def get_dataframes(config: Config) -> tp.Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """Получаем датафреймы из конфига.

    Args:
        config (Config): объект с конфигами

    Returns:
        3 датафрейма
    """
    train_df = pd.read_csv(config.train_dataset_path)
    valid_df = pd.read_csv(config.valid_dataset_path)
    test_df = pd.read_csv(config.test_dataset_path)
    return train_df, valid_df, test_df


def get_datasets(config: Config) -> tp.Tuple[Dataset, Dataset, Dataset]:  # noqa: WPS210
    """Получаем датасеты из конфига.

    Args:
        config (Config): объект с конфигами

    Returns:
        3 датасета
    """
    train_df, valid_df, test_df = get_dataframes(config)

    train_dataset = PlanetDataset(
        train_df,
        config,
        augmentation=augmentations,
        preprocessing=preprocess_imagenet,
    )
    valid_dataset = PlanetDataset(
        valid_df,
        config,
        augmentation=None,
        preprocessing=preprocess_imagenet,
    )
    test_dataset = PlanetDataset(
        test_df,
        config,
        augmentation=None,
        preprocessing=preprocess_imagenet,
    )

    return train_dataset, valid_dataset, test_dataset


def get_dataloaders(config: Config) -> tp.Tuple[tp.Dict[str, DataLoader], DataLoader]:  # noqa: WPS210
    """Получаем даталоадеры из конфига.

    Args:
        config (Config): объект с конфигами

    Returns:
        словарь с датафреймами
    """
    train_dataset, valid_dataset, test_dataset = get_datasets(config)

    train_dataloader = DataLoader(
        train_dataset,
        config.batch_size,
        shuffle=True,
        num_workers=config.num_workers,
        worker_init_fn=worker_init_fn,
    )
    valid_dataloader = DataLoader(
        valid_dataset,
        config.batch_size,
        shuffle=False,
        num_workers=config.num_workers,
    )
    test_dataloader = DataLoader(
        test_dataset,
        config.batch_size,
        shuffle=False,
        num_workers=config.num_workers,
    )

    return {'train': train_dataloader, 'valid': valid_dataloader}, test_dataloader  # noqa: WPS210
