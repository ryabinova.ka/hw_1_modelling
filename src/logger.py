from clearml import Task
from clearml.logger import Logger

from src.config import Config


def get_logger(config: Config) -> Logger:
    """Создаем эксперимент в clearml и подключаемся к нему.

    Args:
        config (Config): объект с конфигами

    Returns:
        логгер
    """
    task = Task.init(
        project_name=config.project_name,
        task_name=config.experiment_name,
    )
    task.connect(config.to_dict())
    return task.get_logger()
