from datetime import datetime
from functools import partial

import albumentations as albu
import torch
from torch.nn import BCEWithLogitsLoss
from torch.optim.lr_scheduler import ReduceLROnPlateau

from src.base_config import Config
from src.train_utils import preprocess_imagenet

SEED = 25
IMG_SIZE = 256
BATCH_SIZE = 32
N_EPOCHS = 10

augmentations = albu.Compose([
    albu.HorizontalFlip(p=0.5),
    albu.VerticalFlip(p=0.5),
    albu.HueSaturationValue(
        hue_shift_limit=20,  # noqa: WPS432
        sat_shift_limit=30,  # noqa: WPS432
        val_shift_limit=20,  # noqa: WPS432
        p=0.5,  # noqa: WPS432
    ),
    albu.RandomBrightnessContrast(
        brightness_limit=0.2,  # noqa: WPS432
        contrast_limit=0.2,  # noqa: WPS432
        p=0.5,  # noqa: WPS432
    ),
    albu.ShiftScaleRotate(),
    albu.GaussianBlur(),
])

config = Config(
    num_workers=4,
    seed=SEED,
    loss=BCEWithLogitsLoss(),
    optimizer=torch.optim.Adam,
    optimizer_kwargs={
        'lr': 1e-3,
        'weight_decay': 5e-4,
    },
    scheduler=ReduceLROnPlateau,
    scheduler_kwargs={
        'mode': 'min',
        'factor': 0.1,
        'patience': 5,
    },
    img_size=IMG_SIZE,
    augmentations=augmentations,
    preprocessing=partial(preprocess_imagenet, img_size=IMG_SIZE),
    batch_size=BATCH_SIZE,
    n_epochs=N_EPOCHS,
    early_stop_patience=10,
    model_kwargs={'name': 'mobilenetv3_small_075', 'pretrained': True},
    log_metrics=['auc', 'f1'],
    binary_thresh=0.1,
    valid_metric='auc',
    minimize_metric=False,
    images_dir='../dataset_planet/train_images',
    train_dataset_path='../dataset_planet/train_df.csv',
    valid_dataset_path='../dataset_planet/valid_df.csv',
    test_dataset_path='../dataset_planet/test_df.csv',
    valid_ratio=0.1,
    project_name='[Classification] hw1',
    experiment_name=f'exp_{datetime.now().strftime("%Y-%m-%d_%H:%M:%S")}',  # noqa: WPS237, E501
    checkpoints_dir='checkpoints',
    traced_model_name='traced_model.pth',
    continue_from=None,
)
