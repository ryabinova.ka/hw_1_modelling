import os
import random

import cv2
import numpy as np
import torch

MAX_PIXEL_INTENSITY = 255


def preprocess_imagenet(im: np.ndarray, img_size: int) -> np.ndarray:
    """Preprocess image as it is done in imagenet.

    Args:
        im (np.ndarray): image
        img_size (int): image_size

    Returns:
        preprocessed image
    """
    im = im.astype(np.float32)
    im /= MAX_PIXEL_INTENSITY
    im = cv2.resize(im, (img_size, img_size))
    im = np.transpose(im, (2, 0, 1))
    im -= np.array([0.485, 0.456, 0.406])[:, None, None]
    im /= np.array([0.229, 0.224, 0.225])[:, None, None]
    return im


def set_global_seed(seed: int) -> None:
    """Set all possible seeds for reproducibility.

    Args:
        seed (int): seed itself
    """
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def worker_init_fn(worker_id: int, initial_seed: int = 42) -> None:
    """Fixes bug with identical augmentations.

    More info: https://tanelp.github.io/posts/a-bug-that-plagues-thousands-of-open-source-ml-projects/  # noqa: E501

    Args:
        worker_id (int): worker id
        initial_seed (int): seed
    """
    seed = initial_seed ** 2 + worker_id
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
